FROM nginx

COPY build.tar build.tar
RUN ls -lah
RUN tar xvf build.tar -C /usr/share/nginx/html

EXPOSE 80

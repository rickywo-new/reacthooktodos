import * as React from 'react';
import { Container, makeStyles, TextField } from '@material-ui/core';

export namespace TodoInput {
  export interface Props {
    text?: string;
    helperText?: string;
    newTodo?: boolean;
    editing?: boolean;
    onSave: (text: string) => void;
  }
}

const useStyles = makeStyles((theme) => ({
  textFieldWrapper: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  }
}));

export const TodoInput: React.FunctionComponent<TodoInput.Props> = (
  props: TodoInput.Props,
  context?: any
) => {
  const classes = useStyles();
  const [text, setText] = React.useState(props.text || '');

  const handleSubmit = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.which === 13) {
      props.onSave(text);
      if (props.newTodo) {
        setText('');
      }
    }
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    const text = event.target.value.trim();
    if (!props.newTodo) {
      props.onSave(text);
    }
  };

  return (
    <Container className={classes.textFieldWrapper}>
      <TextField
        label="Task"
        margin="normal"
        autoFocus
        value={text}
        fullWidth
        helperText={props.helperText ? props.helperText : ''}
        onBlur={handleBlur}
        onChange={handleChange}
        onKeyDown={handleSubmit}
      />
    </Container>
  );
};

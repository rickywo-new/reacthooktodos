import * as React from 'react';
import { TodoModel } from 'app/models';
import { Badge, Button, ButtonGroup, createStyles, makeStyles, Theme } from '@material-ui/core';

export const FILTER_TITLES = {
  [TodoModel.Filter.SHOW_ALL]: 'All',
  [TodoModel.Filter.SHOW_ACTIVE]: 'Active',
  [TodoModel.Filter.SHOW_COMPLETED]: 'Completed'
};

export namespace Toolbar {
  export interface Props {
    filter: TodoModel.Filter;
    activeCount?: number;
    completedCount?: number;
    onClickFilter: (filter: TodoModel.Filter) => any;
    onClickClearCompleted: () => any;
  }
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      justifyContent: 'space-between',
      padding: theme.spacing(2)
    },
    padding: {
      padding: theme.spacing(0, 2)
    }
  })
);

export const Toolbar: React.FunctionComponent<Toolbar.Props> = (props: Toolbar.Props) => {
  const [activeFilter, setActiveFilter] = React.useState(TodoModel.Filter.SHOW_ALL);
  const styledClasses = useStyles();

  const filterButtonGroup = React.useMemo(
    () => {
      const renderFilterLink = (filter: TodoModel.Filter): JSX.Element => {
        const { onClickFilter } = props;

        return (
          <a
            onClick={() => {
              setActiveFilter(filter);
              onClickFilter(filter);
            }}
            children={FILTER_TITLES[filter]}
          />
        );
      };
      const buttons = (Object.keys(TodoModel.Filter) as (keyof typeof TodoModel.Filter)[]).map(
        (key) => (
          <Button
            key={TodoModel.Filter[key]}
            variant={TodoModel.Filter[key] === activeFilter ? 'contained' : 'outlined'}
          >
            {TodoModel.Filter[key] === TodoModel.Filter.SHOW_ACTIVE ? (
              <Badge
                className={styledClasses.padding}
                color="secondary"
                badgeContent={props.activeCount}
              >
                {renderFilterLink(TodoModel.Filter[key])}
              </Badge>
            ) : (
              renderFilterLink(TodoModel.Filter[key])
            )}
          </Button>
        )
      );
      return (
        <ButtonGroup
          color="primary"
          variant="outlined"
          size="small"
          aria-label="small outlined primary button group"
        >
          {buttons}
        </ButtonGroup>
      );
    },
    [TodoModel.Filter, props.activeCount, activeFilter]
  );

  return <div className={styledClasses.toolbar}>{filterButtonGroup}</div>;
};

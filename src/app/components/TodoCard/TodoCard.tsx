import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import * as React from 'react';
import { TodoModel } from 'app/models';
import { TodoActions } from 'app/actions';
import { TodoInput } from '../TodoInput';
import styled from '@emotion/styled';
import { IconButton } from '@material-ui/core';
import { Delete, CheckCircle, CheckCircleOutline } from '@material-ui/icons';
import { green } from '@material-ui/core/colors';

const TodoCardContainer = styled.div`
  flex-grow: 1;
  :hover {
    border-left: solid 5px #38ef7d;
    transition-duration: 0.2s;
    transition-timing-function: ease-in;
  }
`;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(2),
      margin: 'auto'
    },
    iconDone: {
      color: green[800]
    }
  })
);

export namespace TodoCard {
  export interface Props {
    todo: TodoModel;
    editTodo: typeof TodoActions.editTodo;
    deleteTodo: typeof TodoActions.deleteTodo;
    completeTodo: typeof TodoActions.completeTodo;
  }

  export interface State {
    editing: boolean;
  }
}

export const TodoCard: React.FunctionComponent<TodoCard.Props> = (
  props: TodoCard.Props,
  context?: any
) => {
  const [editing, setEditing] = React.useState(false);
  const handleDoubleClick = () => {
    setEditing(true);
  };
  const handleSave = (id: number, text: string) => {
    if (text.length === 0) {
      props.deleteTodo(id);
    } else {
      props.editTodo({ id, text });
    }
    setEditing(false);
  };

  const { todo, completeTodo, deleteTodo } = props;
  const styledClasses = useStyles();

  const content = editing ? (
    <TodoInput
      text={todo.text}
      editing={editing}
      onSave={(text) => todo.id && handleSave(todo.id, text)}
    />
  ) : (
    <div>
      <IconButton
        aria-label="check"
        // color={todo.completed ? 'primary' : 'default'}
        onClick={() => todo.id && completeTodo(todo.id)}
      >
        {todo.completed ? (
          <CheckCircle className={styledClasses.iconDone} />
        ) : (
          <CheckCircleOutline />
        )}
      </IconButton>
      <label onDoubleClick={() => handleDoubleClick()}>{todo.text}</label>
    </div>
  );

  return (
    <TodoCardContainer>
      <Paper className={styledClasses.paper} square={true}>
        <Grid item xs container spacing={2}>
          <Grid item xs>
            {content}
          </Grid>
          <Grid item>
            <IconButton
              aria-label="check"
              color="secondary"
              onClick={() => {
                if (todo.id) deleteTodo(todo.id);
              }}
            >
              <Delete />
            </IconButton>
          </Grid>
        </Grid>
      </Paper>
    </TodoCardContainer>
  );
};

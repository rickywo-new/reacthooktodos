import * as React from 'react';
import { TodoActions } from 'app/actions/todos';
import { TodoCard } from '../TodoCard';
import { TodoModel } from 'app/models/TodoModel';

export namespace TodoList {
  export interface Props {
    todos: TodoModel[];
    actions: TodoActions;
  }
}

export const TodoList: React.FC<TodoList.Props> = ({ todos, actions }: TodoList.Props) => {
  return (
    <section>
      {todos.map((todo: TodoModel) => (
        <TodoCard
          key={todo.id}
          todo={todo}
          completeTodo={actions.completeTodo}
          deleteTodo={actions.deleteTodo}
          editTodo={actions.editTodo}
        />
      ))}
    </section>
  );
};

# Frontend Boilerplate with React, Redux & TypeScript


A bare minimum react-redux-webpack-typescript boilerplate with TodoMVC example.
Implemented with React Hook and Functional Component.

[Live Demo](http://review-develop-3zknud.reacthooktodos.35.201.21.5.nip.io/)

Note that this project does not include **Server-Side Rendering**, **Static code analysis**, **Testing Frameworks** and other stuffs that makes the package unnecessarily complicated. (e.g. yarn, tslint, jest, ...)  
If needed, please fork this repository and add your own that meets your requirements.

Ideal for creating React apps from the scratch.

See also: [react-mobx-typescript-boilerplate](https://github.com/rokoroku/react-mobx-typescript-boilerplate)

## Contains

- [x] [Typescript](https://www.typescriptlang.org/) 3
- [x] [React](https://facebook.github.io/react/) 16.8
- [x] [Redux](https://github.com/reactjs/redux) 4
- [x] [Material-UI](https://material-ui.com/) 4
- [x] [Emotion](https://emotion.sh/) 10
- [x] [React Router](https://github.com/ReactTraining/react-router) 4.3
- [x] [Redux DevTools Extension](https://github.com/zalmoxisus/redux-devtools-extension)
- [x] [Docker](https://www.docker.com/)

### Build tools

- [x] [Webpack](https://webpack.github.io) 4
  - [x] [Tree Shaking](https://medium.com/@Rich_Harris/tree-shaking-versus-dead-code-elimination-d3765df85c80)
  - [x] [Webpack Dev Server](https://github.com/webpack/webpack-dev-server)
- [x] [Typescript Loader](https://github.com/TypeStrong/ts-loader)
- [x] [PostCSS Loader](https://github.com/postcss/postcss-loader)
  - [x] [PostCSS Preset Env](https://preset-env.cssdb.org/)
  - [x] [CSS modules](https://github.com/css-modules/css-modules)
- [x] [React Hot Loader](https://github.com/gaearon/react-hot-loader)
- [x] [Mini CSS Extract Plugin](https://github.com/webpack-contrib/mini-css-extract-plugin)
- [x] [HTML Webpack Plugin](https://github.com/ampedandwired/html-webpack-plugin)

## Installation

```
$ npm ci
```

## Running

```
$ npm start
```

## Build

```
$ npm run build
```

## Deploy (to the [GitHub Pages](https://pages.github.com/))

```
$ npm run deploy
```

## CI/CD Integration (via [Gitlab](https://gitlab.com/))

GCP Kubernetes Auto-deployment
Add the following variables key and values:

- KUBE_NAME: cluster name
- KUBE_USER: kubernetes cluster admin user
- KUBE_PASSWORD: kubernetes cluster admin password
- GCP_PROJECT_ID: Google Cloud Project Id (or name)
- GCP_SERVICE_KEY: Your Service Account credentials JSON (the one we’ve created and downloaded at the first step). You can copy the entire structure of the file here:

```
{
  "type": "service_account",
  "project_id": "sample-project-id",
  "private_key_id": "xxxxx",
  "private_key": "-----BEGIN PRIVATE KEY-----\n-----END PRIVATE KEY-----\n",
  "client_email": "email@iam.gserviceaccount.com",
  "client_id": "1234567890",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://accounts.google.com/o/oauth2/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": ""
}
```

- GCP_APP_NAME: the name of you application or image (eg. my-sample-app, my-sample-image)
- MY_DOMAIN: your domain (eg. sample-project.my.custom.domain.com)
- USE_SSL: True or False (depending on if you are planning to use https for your deployment)

## Format code (using [Prettier](https://github.com/prettier/prettier))

```
$ npm run prettier
```

# License

MIT
